%% Set up send

% instantiate the library
disp('Loading library...');
lib = lsl_loadlib();


disp('Creating a new marker stream info...');
info = lsl_streaminfo(lib,'MyMarkerStream','Markers',1,0,'cf_string','myuniquesourceid23443');

disp('Opening an outlet...');
outlet = lsl_outlet(info);

%% Set up recieve

% resolve a stream...
disp('Resolving a Markers stream...');
result = {};
while isempty(result)
    result = lsl_resolve_byprop(lib,'type','Markers'); end

% create a new inlet
disp('Opening an inlet...');
inlet = lsl_inlet(result{1});

disp('Now receiving data...');

%% Send markers

% send markers into the outlet
disp('Now transmitting data...');
markers = {'Test'};
time1=[];

pause(10);

while true
    pause(0.1);
    mrk = markers{1};
    disp(['now sending ' mrk]);
    timestamps = lsl_local_clock(info.LibHandle);
    outlet.push_sample({mrk});   % note that the string is wrapped into a cell-array
    
    fprintf('send stamp at %.5f\n',timestamps);
    
    time1=[time1; [timestamps]];
    
end
