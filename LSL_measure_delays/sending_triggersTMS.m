


pulse_duration = 0.001;
on_value = ([1 1 1 1]);
off_value =([0 0 0 0 ]);

daqregister('parallel')
paraport=digitalio('parallel','LPT1');
line1=addline(paraport,0:3,'out');
putvalue(paraport,off_value);

% Trigger times are defined by a vector called 'data', which gives
% the times, in seconds, of when to produce the trigger.  For example
% data = [ 1.25 3.0 5.6];
% will produce a trigger at 1.25 sec, 3 sec, and 5.6 seconds.
% data should be monotonically increasing.
%
%  The file pulses.m shows how to create the data vector and save it to a
%  file.  It can then be read in with the function dlmread.  For example,
%  data =dlmread('pulses.txt');
%  but this step can be skipped if you want to just create the data vecotr
%  here.


% data =dlmread('pulses.txt'); % read the data vector in from a file.
% data = 1:2:160;  % produce a trigger every 2 seconds for 160 seconds
%data = cumsum(1+rand(1,10));  produce 10 triggers with delays between 1 and 2 seconds
% data = cumsum(1+2*rand(1,10));  %produce 10 triggers with delays between 1 and 3 seconds
data = cumsum(3+0.5*rand(1,240));  %produce 80 triggers with delays between 3 and 3.5 seconds


cnt = 1;
lng = length(data);
lngstr = num2str(lng);
disp('Press any key to begin')
pause
fprintf(1,'Here we go!  Press ctrl-c to stop\n');
tic;

while cnt <=lng
    now = toc;
    if now >=data(cnt)
        % send trigger
        
        putvalue(paraport,on_value)
        pause(pulse_duration);
        putvalue(paraport,off_value)
        fprintf(1, 'Trigger %d (%.3f sec ) of %s (%.3f sec)\n', cnt, data(cnt),  lngstr, data(end));
        
        cnt = cnt+1;
    end
end
disp('Fini')