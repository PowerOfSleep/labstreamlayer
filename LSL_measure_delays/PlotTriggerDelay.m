time1=time1(1:end-5);
jitter = time2-time1;
jitter=jitter*1000;

Maxjitter = max(jitter);
Meanjitter = mean(jitter);


%% All triggers
figure
h = histogram(jitter);
title('Matlab to LSL to Matlab; Delay jitter')
xlim([0 0.6])
yL = get(gca,'YLim');
line([Meanjitter Meanjitter],yL,'Color','r','LineWidth',1.5);
xlabel('millisec') % x-axis label
ylabel('#') % y-axis label


%% Last 200

figure
h = histogram(jitter(end-200:end));
title('Matlab to LSL to Matlab; Delay last 200 triggers')
xlabel('millisec') % x-axis label
ylabel('#') % y-axis label

%% First 200

figure
h = histogram(jitter(1:200));
title('Matlab to LSL to Matlab; Delay first 200 triggers')
xlabel('millisec') % x-axis label
ylabel('#') % y-axis label